angular.module('MainApp', ['ui.router', 'ui.bootstrap'])
	.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'partials/home.html',
				controller: 'ViewController as view',
				data: {url: 'beerHeader.jpg', height:'650px', offset: '-350px'}
			})
			.state('resume', {
				url: '/resume',
				templateUrl: 'partials/resume.html',
				controller: 'ViewController as view',
				data: {url: 'treesHeader.jpg', height: '950px', offset: '-650px'}
			})
			.state('portfolio', {
				url: '/portfolio',
				templateUrl: 'partials/portfolio.html',
				controller: 'ViewController as view',
				data: {url:'purpleBulbHeader2.jpg', height: '750px', offset: '-450px'}
			})
			.state('notfound', {
				url:"/notfound",
				templateUrl: 'partials/notfound.html'
			});

			$urlRouterProvider.otherwise('/notfound');

			$locationProvider.html5Mode(true);

	}]);