var dsSlideshow = function(){

	return{
		templateUrl: 'partials/sildeshow.html',
		scope: {
			type: '@',
			format: '@'
		},
		link: function(scope, elem, attrs, controller){
			controller.init(scope.type);
		},
		controller: 'SlideShowController',
		controllerAs: 'slideshow'
	}

	

}

angular.module('MainApp').directive('dsSlideshow', dsSlideshow);