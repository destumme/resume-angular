var MainController = ['$state', '$scope', function($state, $scope){

	var self = this;
	self.navbarCollapsed = true;

}];

var ViewController = ['$state', '$scope', function($state, $scope){
	$scope.$parent.main.header = $state.current.data;
	$scope.$parent.main.navbarCollapsed = true;

}];

var SlideShowController = ['$scope', '$http', '$modal', function($scope, $http, $modal){

	var self = this;
	self.format = $scope.format;

	this.init = function(type){

		var uri = 'images/' + type + '.json';

		$http.get(uri).success(function(data){
			self.images = data;
			self.active = 0;
		});

	}

	this.next = function(){
		if(self.active < self.images.length - 1){
			self.active = self.active + 1;
		} else {
			self.active = 0;
		}
		
	}

	this.prev = function(){
		if(self.active == 0){
			self.active = self.images.length - 1;
		} else {
			self.active = self.active - 1;
		}
		
	}

	this.maximize = function(image){

	$modal.open({
		animation: true,
		templateUrl: 'partials/imageModal.html',
		controller: 'ModalController',
		controllerAs: 'modal',
		size: 'lg',
		resolve: {
			list : function(){
				return {'images': self.images, 'image':image};
			}
		}
	});

	}

}];

var ModalController = ['$scope', '$modalInstance', 'list', function($scope, $modalInstance, list){
	var self = this;
	this.image = list.image;
	this.images = list.images;

	this.close = function(){
		$modalInstance.dismiss();
	}

	this.left = function(){
		var i = self.images.indexOf(self.image);
		if(i == 0){
			self.image = self.images[self.images.length - 1];
		} else {
			self.image = self.images[i-1];
		}
	}
	this.right = function(){
		var i = self.images.indexOf(self.image);
		if(i == self.images.length - 1){
			self.image = self.images[0];
		} else {
			self.image = self.images[i+1];
		}
	}
}];

angular.module('MainApp')
	.controller('MainController', MainController)
	.controller('ViewController', ViewController)
	.controller('SlideShowController', SlideShowController)
	.controller('ModalController', ModalController)
;